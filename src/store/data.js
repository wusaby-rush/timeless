import { reactive } from "petite-vue"
import { arrayUnion, doc, setDoc } from "firebase/firestore"
import { props, process } from '../store'

const data = reactive({
    saveEntry(entry) {
        if (props.user) {
            const entryRef = doc(db, props.contact, `${entry.timestamp}`)
            const prefsRef = doc(db, 'users', props.user)
            
            // update tags
            setDoc(prefsRef, {
                tags: arrayUnion(...entry.tags)
            }, { merge: true })
            .catch(err => {
                process.fail(err)
            })

            // add period to db
            setDoc(entryRef, entry)
            .then(() => {
                let stage = entry.stage === 'break' ? 'الاستراحة' : 'الفترة'
                process.success(`تم تسجيل ${stage}`)
            })
            .catch(err => {
                process.fail(err)
            })
        } else {
            let userStats = JSON.parse(localStorage.getItem('statistics')) || []
            userStats.push(entry)
            localStorage.setItem('statistics',JSON.stringify(userStats))

            let userTags = JSON.parse(localStorage.getItem('tags')) || []
            userTags.push(...entry.tags)
            localStorage.setItem('tags',JSON.stringify(userTags))
        }
    }
})

export default data
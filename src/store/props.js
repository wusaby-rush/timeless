import { reactive } from "petite-vue"

const props = reactive({
    // user data
    get contact() {
        return localStorage.getItem('contact')
    },
    set contact(contact) {
        localStorage.setItem('contact', contact)
        if (!contact)
            localStorage.removeItem('contact')
    },
    get user() {
        return localStorage.getItem('user')
    },
    set user(user) {
        localStorage.setItem('user', user)
        if (!user)
            localStorage.removeItem('user')
    },
    
    // user prefs
    get ringtone() {
        return localStorage.getItem('ringtone') || 'tone';
    },
    set ringtone(value) {
        localStorage.setItem('ringtone', value)
        if (!value)
            localStorage.removeItem('ringtone')
    }, 
})

export default props
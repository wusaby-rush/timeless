import { reactive } from "petite-vue"

const process = reactive({
    // loading data
    loading: false,
    toast: false,
    toastMessage: "",

    startProcess() {
        this.loading = true
    },
    success(message) {
        this.loading = false
        this.showToast(message)
        console.log(message)
    },
    fail(error) {
        this.loading = false
        this.showToast(error.message)
        console.warn(error.message)
    },
    showToast(message) {
        this.toast = true
        this.toastMessage = message
        setTimeout(() => {
            this.toast = false
        }, 3000)
    }, 
})

export default process
const staticCacheName = 'timeless-static-v0.2.8'
let assets = new Set(['/', '/index.html'])

// fill assets
const precacheManifest = [body]
precacheManifest[0].map(asset => assets.add(asset.url))

// install sw (precache)
self.addEventListener('install', event => {
  event.waitUntil(
    caches
    .open(staticCacheName)
    .then(cache => {
      return cache.addAll(assets)
    })
  )
  console.log('service worker installed')
})

// activate sw (remove unwanted caches)
self.addEventListener('activate', event => {
  event.waitUntil(
  	caches.keys()
    .then(cacheNames => {
      return Promise.all(
        cacheNames
        .filter(cacheName => { 
          return cacheName.startsWith('timeless-') && cacheName != staticCacheName
         })
        .map(cacheName => caches.delete(cacheName))
      )
    })
  )
})

// serve app shell
self.addEventListener('fetch', evt => {
  if (evt.request.url.indexOf('timeless.wusaby.me') > -1) {
    evt.respondWith(
    	caches.match(evt.request).then(cacheRes => cacheRes)
    )
  }
})
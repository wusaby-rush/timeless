window.addEventListener('click', () => {
    let sound = document.querySelector('audio');

    sound.play();
    sound.onended = () => {
        sound.muted = false;
        sound.volume = 1;
    }
}, {once: true});
import { collection, query, where, getDocs, deleteDoc } from 'firebase/firestore'
import { Chart, registerables } from 'chart.js'
import { props, process } from '../store'
Chart.register(...registerables)

let myChart

function stats() {
    return {
    collection: null,
    interval: '86400',
    data: {
        tasks: { all: 0, work: 0, study: 0, sport:0 },
        breaks: { all: 0, work: 0, study: 0, sport:0 },
    },

    updateData() {
        // initilaze data
        if (props.user) {
            process.startProcess()
            const dataRef = collection(db, props.contact)
            let time = (Date.now() / 1000) - this.interval
            const q = query(dataRef, where("timestamp", ">=", time))
            getDocs(q)
            .then( snpashot => {
                snpashot.docs.forEach(doc => {
                    let stat = doc.data()
                    if (stat.stage === 'task')
                        this.props.tasks[stat.category] += stat.period / 60 / 60
                    else if (stat.stage === 'break')
                        this.props.breaks[stat.category] += stat.period / 60 / 60

                })

                // calculate total tasks and breaks
                this.props.tasks.all = this.props.tasks.work + this.props.tasks.study + this.props.tasks.sport
                this.props.breaks.all = this.props.breaks.work + this.props.breaks.study + this.props.breaks.sport

                // check if data empty
                if (this.props.tasks.all === 0 && this.props.breaks.all === 0)
                    throw new Error('لا توجد بيانات')

                process.success('حُدثت البيانات')
            })
            .catch(error => {
                process.fail(error)
            })
        }
    },
    clearData() {
        if (!confirm('هل ترغب فعلا في حذف كافة الببيانات؟ بمجرد حذفها لن تكون قادر على استعادتها'))
            return

        localStorage.removeItem('statistics')

        // clear firebase data
        if (props.user) {
            process.startProcess()
            const dataRef = collection(db, props.contact)
            getDocs(dataRef)
            .then(snapshot => {
                snapshot.docs.forEach(doc => {
                    deleteDoc(doc.ref)
                })
            })
            .then(() => {
                // clear data
                this.data = {
                    tasks: { all: 0, work: 0, study: 0, sport:0 },
                    breaks: { all: 0, work: 0, study: 0, sport:0 },
                }
                props.success('حُذفت البيانات')
            })
            .catch(error => {
                props.fail(error)
            })
        }
    },

    renderCharts() {
        let data = this.data
        const ctx = this.$refs.bar.getContext('2d')
        var yValues = [props.tasks.work, props.breaks.work, props.tasks.study, props.breaks.study, props.tasks.sport, props.breaks.sport];
        if (myChart instanceof Chart) {
            myChart.destroy()
        }

        myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['مهام العمل', 'استراحات العمل', 'مهام الدراسة', 'استراحات الدراسة', 'مهام الرياضة', 'استراحات الرياضة'],
                datasets: [{
                    label: '# الاحصائيات',
                    data: yValues,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });
    }
}
}

export default stats
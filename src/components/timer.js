import { data, props } from "../store"

function timer(tags) { 
    return {
    hr: 0,
    min: 0,
    sec: 0,
    elapsedTime: 0,
    period: 0,
    limitCounter: 0,
    timerOn: false,
    stage: 'task',
    category: 'work',
    isTagsOpen: false,
    selectedTags: [],
    tags,
    timerInterval: null,
    sound: document.querySelector('audio'),

    get now() {
        return Math.floor(Date.now() / 1000)
    },

    hours() {
        return (this.hr>0)? this.hr.toLocaleString('en-US', {minimumIntegerDigits: 2, useGrouping: false})+':' : '';
    },
    minutes() {
        return this.min.toLocaleString('en-US', {minimumIntegerDigits: 2, useGrouping: false});
    },
    seconds() {
        return this.sec.toLocaleString('en-US', {minimumIntegerDigits: 2, useGrouping: false});
    },
    // start or resume timer for any stage
    start() {
        this.timerOn = true;
        if (this.stage !== 'break')
            this.taskTime();
        else
            this.breakTime();
    },
    // stop timer for any stage, can be resumed by start
    stop() {
        this.timerOn = false
        clearInterval(this.timerInterval)
    },
    // calculate timer ascending from 0 to upper limit
    taskTime() {
        let startTime = this.now - this.elapsedTime;
        this.timerInterval = setInterval(() => {
            this.elapsedTime = this.now - startTime;
            this.hr = Math.floor(this.elapsedTime / 60 / 60);
            this.min = Math.floor(this.elapsedTime / 60 % 60);
            this.sec = Math.floor(this.elapsedTime % 60);
        }, 1000);
    },
    // calculate timer descending from breaktime and 0, then change stage automatically
    breakTime() {
        let endTime = this.now + (this.elapsedTime || this.limitCounter)
        this.timerInterval = setInterval(() => {
            this.elapsedTime = endTime - this.now
            this.hr = Math.floor(this.elapsedTime / 60 / 60)
            this.min = Math.floor(this.elapsedTime / 60 % 60)
            this.sec = Math.floor(this.elapsedTime % 60)
            if (this.now >= endTime) {
                this.sound.play()
                // if (Notification.permission === 'granted') {
                //     new Notification('Time is up!', {
                //         body: 'Take a break!'
                //     });
                // }
                this.forward()
            }
        },1000)
    },
    // change stage, reset, change timer and store data
    async forward() {
        if (this.stage === 'task')
            // this.period = this.elapsedTime
            // get period, if task less than 15 mins, zero it
            this.period = (this.elapsedTime < 15*60) ? 0 : this.elapsedTime
        else 
            // get period, if not finish calculate it by limitCounter - elapsedTime
            this.period = (this.elapsedTime <= 0)? this.limitCounter : this.limitCounter - this.elapsedTime

        // stop timer
        this.timerOn = false
        clearInterval(this.timerInterval)
        this.elapsedTime = 0

        if (this.stage === 'break')
            this.save()
        
        if (this.stage === 'task' && this.period) {
            this.limitCounter = Math.floor(this.period / 5)
            this.isTagsOpen = true
        } else {
            this.stage = 'task'
            this.selectedTags = []
            this.period = 0
            this.hr = 0
            this.min = 0
            this.sec = 0
        }
    },
    save() {
        if (this.period)
            data.saveEntry({
                stage: this.stage,
                category: this.category,
                period: this.period,
                tags: this.selectedTags,
                timestamp: this.now,
            })
    }
    }
}

export default timer
import { doc, setDoc } from 'firebase/firestore'
import {
    createUserWithEmailAndPassword,
    signInWithEmailAndPassword,
    signOut,
} from 'firebase/auth';
import { process } from '../store';

function authn() {
    return {
    form: 'login',
    email: null,
    passwd: null,

    signout() {
        if (!confirm('هل ترغب فعلا في تسجيل الخروج؟'))
            return
        signOut(auth)
    },

    signin() {
        process.startProcess()
        signInWithEmailAndPassword(auth, this.email, this.passwd)
        .then( cred => {
            this.syncLocalData()
            process.success('تم تسجيل الدخول')
        })
        .catch(error => {
            process.fail(error)
        })
        .finally(() => {
            this.email = ''
            this.passwd = ''
        })
    },

    signup() {
        process.startProcess()
        createUserWithEmailAndPassword(auth, this.email, this.passwd)
        .then(cred => {
            this.syncLocalData()
            process.success('تم انشاء حساب جديد')
        })
        .catch(error => {
            process.fail(error)
        })
        .finally(() => {
            this.email = ''
            this.passwd = ''
        })
    },

    syncLocalData() {
        // sync previous local data
        let statistics = JSON.parse(localStorage.getItem('statistics')) || []
        let doesntUploaded = statistics.filter((stat) => {
            setDoc(doc(db, user.contact, `${stat.timestamp}`), stat)
            .then(_ => {
                return false
            })
            .catch(error => {
                return true
            })
        })
        localStorage.setItem('statistics', JSON.stringify(doesntUploaded))
    }
}
}

export default authn
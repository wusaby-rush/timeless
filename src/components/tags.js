import { doc, getDoc, setDoc } from "firebase/firestore"
import { process, props } from "../store"

const tags = {
    input: '',
    loading: false,
    allTags: [],
    done() {
        this.save()
        this.stage = 'break'
        this.start()
        this.isTagsOpen = false
    },
    async getTags() {
        this.loading = true
        try {
            if (props.user) {
                const tagsRef = doc(db, 'users', props.user)
                const tags = await getDoc(tagsRef)
                this.allTags = tags.data().tags
            } else {
                const tags = JSON.parse(localStorage.getItem('tags')) || []
                this.allTags = tags
            }
        } catch (err) {
            process.fail(err)
        }
        finally {
            this.loading = false
        } 
    },
    get filterdTags() {
        return this.allTags.filter(tag => tag.includes(this.input.toLowerCase()))
    },
    addTags() {
        let tags = this.input
        ?.toLowerCase()
        ?.trim()
        ?.split(/\s+/)
        .filter(Boolean)

        tags = [...new Set(tags)]
        this.allTags.push(...tags)
        this.selectedTags.push(...tags)
        this.input = ''
    },
    deleteTag(tag) {
        const confirm = window.confirm(`هل أنت متأكد من حذف الوسم "#${tag}"؟\nفي حال حذفه فلن تظهر أي من البيانات المرتبطة به في الاحصائيات`)
        if (confirm) {
            this.allTags = this.allTags.filter(t => t !== tag)
            if (props.user) {
                const tagsRef = doc(db, 'users', props.user)
                setDoc(tagsRef, {
                    tags: this.allTags
                }, { merge: true })
                .catch(err => {
                    process.fail(err)
                })
            } else {
                localStorage.setItem('tags', JSON.stringify(this.allTags))
            }
        }
    }
}

export default tags
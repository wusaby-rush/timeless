/* 
    * LATER:
    * use favicon as in Vite-PWA
    * complete PWA support
    * use workers
    * use i18n
    * redesign the stitiscs UI
*/

import './style.css'
import './js/APIpollyfils'

// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
//import { getAnalytics } from "firebase/analytics";
import { getFirestore, enableIndexedDbPersistence } from "firebase/firestore";
import { getAuth, onAuthStateChanged } from "firebase/auth";

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyCrWidKOUwoX2c_iWAH5PYldcz04sdzP6c",
    authDomain: "timeless-4181d.firebaseapp.com",
    projectId: "timeless-4181d",
    storageBucket: "timeless-4181d.appspot.com",
    messagingSenderId: "753530810362",
    appId: "1:753530810362:web:4443b602de00d73f0efe43",
    measurementId: "G-FCQGJBLGS7"
};
  
// Initialize Firebase
const firebase = initializeApp(firebaseConfig);
const db = getFirestore(firebase);
enableIndexedDbPersistence(db)
  .catch((err) => {
      if (err.code == 'failed-precondition') {
          process.fail(new Error("التطبيق مفتوح في اكثر من صفحة، فلن يعمل بدون اتصال انترنت"))
      } else if (err.code == 'unimplemented') {
          process.fail(new Error('جهازك لا يدعم تخزين البيانات بدون اتصال'))
      }
  });
//getAnalytics(firebase);
const auth = getAuth(firebase);
window.db = db
window.auth = auth

import { createApp } from 'petite-vue'

import authn from './components/authn'
import timer from './components/timer'
import stats from './components/stats'
import tags from './components/tags'
import { data, process, props } from './store'

// get user interactivly
onAuthStateChanged(auth, user => { // if logged out, user will be null
    if (!user) {
      props.user = null
      props.contact = null
        return
    }
  
    props.user = user.uid
    props.contact = user.email
})

// initilaze petite-vue
createApp({
    authn,
    timer,
    stats,
    tags,
    data,
    process,
    props
}).mount()

// prepare notifcation settings
// if (Notification.permission !== 'granted') {
//     Notification.requestPermission();
// }
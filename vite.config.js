// vite.config.js
import { defineConfig } from 'vite'
import { VitePWA } from 'vite-plugin-pwa'

export default defineConfig({
  plugins: [
    VitePWA({
      injectRegister: 'inline', // where inject the register code
      strategies: 'injectManifest', // generate all sw or just inject assets
      srcDir: './js', // the source directory
      injectManifest: {
        globPatterns: ['**/*.{js,css,png,jpg,jpeg,svg,gif,webp,mp4,webm,ogg,mp3,wav,flac,aac,woff,woff2,ttf,eot}'],
        injectionPoint: 'body', // where inject the assets
      },
      manifest: {
        name: "timeless",
        description: "pomodoro timer",
        start_url: "/",
        icons: [
            {
               src: "assets/icons/512.png",
               type: "image/png",
               sizes: "512x512"
            },
            {
               src: "assets/icons/512.png",
               type: "image/png",
               sizes: "512x512",
               purpose: "any maskable"
            },
            {
               src: "assets/icons/128.png",
               type: "image/png",
               sizes: "128x128"
            }
        ],
        display: "standalone",
        id: "me.wusaby.timeless",
        theme_color: "#ffffff",
        background_color: "#ffffff"
      },
    })
  ],
  root:'src',
  publicDir: '../public',
  build: {
    outDir: '../dist',
    emptyOutDir: true,
  }
})